<?php

// ----0--9--8--7--6--5--4--3--2--1--1--2--3--4--5--6--7--8--9--0---- //
// ================================================================== //
//                                                                    //
//                  WP-Start - WordPress Start Script                 //
//                                                                    //
// ================================================================== //
//                                                                    //
//                      Version 1.0 / 06.12.2020                      //
//                                                                    //
//                      Copyright 2020 - PB-Soft                      //
//                                                                    //
//                           www.pb-soft.com                          //
//                                                                    //
//                           Patrick Biegel                           //
//                                                                    //
// ================================================================== //


// #####################################################################
// ##########      C O N F I G U R A T I O N - B E G I N      ##########
// #####################################################################

// =====================================================================
// Please specify the default language for the WordPress installation.
// Depending on the selection the WordPress archive will be downloaded
// in the specified language. This option will only be used if the
// installation will be started with parameters and without the menu.
//
//   CH => German - Switzerland
//   DE => German - Germany
//   EN => English
//   ES => Spanish
//	 FR => French
//	 IT => Italian
//
// =====================================================================
$default_language = "CH";


// =====================================================================
// Please specify the local path for the ZIP file. The downloaded
// WordPress archive will be stored at this location.
// =====================================================================
$zip_local_path = "wordpress.zip";


// =====================================================================
// Please specify the ZIP extraction directory. The WordPress files will
// be extracted to this location. Normally this should be the directory
// where this script was copied to.
// =====================================================================
$zip_extract_dir = getcwd();


// =====================================================================
// Specify if an existing WordPress installation will be overwritten.
// If enabled, the WordPress archive will be extracted over an existing
// WordPress installation. This can be used to complete an installation
// with missing files but also will overwrite changes made to original
// files after the initial installation of WordPress.
// =====================================================================
$default_overwrite_wp = 0;


// =====================================================================
// Please specify if the ZIP file should be deleted. If this option is
// enabled, the WordPress archive will be deleted after successfully
// extracting the files to the specified directory.
// =====================================================================
$default_delete_zip = 0;


// =====================================================================
// Please specify if you want do display the copyright. Display the
// copyright is not mandatory, I made a configuration setting for you
// to disable it :-)
// =====================================================================
$display_copyright = 1;


// =====================================================================
// Specify the zip source URLs. The download URLs for the WordPress
// archives are specified here. Be sure to use ZIP versions of the
// files and not 'tar.gz' archives!
// =====================================================================
$zip_source_urls['ch'] = "https://de-ch.wordpress.org/latest-de_CH.zip";
$zip_source_urls['de'] = "https://de.wordpress.org/latest-de_DE.zip";
$zip_source_urls['en'] = "https://wordpress.org/latest.zip";
$zip_source_urls['es'] = "https://es.wordpress.org/latest-es_ES.zip";
$zip_source_urls['fr'] = "https://fr.wordpress.org/latest-fr_FR.zip";
$zip_source_urls['it'] = "https://it.wordpress.org/latest-it_IT.zip";


// #####################################################################
// ############      C O N F I G U R A T I O N - E N D      ############
// #####################################################################

// Specify the company information.
$company['name'] = "PB-Soft";
$company['link'] = "https://www.pb-soft.com";

// Specify the product information.
$product['version'] = "1.0";
$product['name'] = "WP-Start";
$product['info'] = "WordPress Start Script";

// Specify the HTML header.
$html_header  = "<!DOCTYPE html>\n";
$html_header .= "<html lang=en>\n";
$html_header .= "<head>\n";
$html_header .= "<title>".$product['name']." - ".$product['info']."</title>\n";
$html_header .= "<style>\n";
$html_header .= "body {background-color: #122a52; color: #122a52; font-family: arial, sans-serif; text-align: center}\n";
$html_header .= "h1 {margin-bottom: 5px}\n";
$html_header .= "h1 > a {color: #122a52}\n";
$html_header .= "h1 > a:hover {color: #3F51B5}\n";
$html_header .= "h2 {font-size: 18px; margin-top: 5px}\n";
$html_header .= "a {text-decoration: none}\n";
$html_header .= "hr {border: 1px solid #acc7f3; margin-top: 20px; width: 90%}\n";
$html_header .= ".box {background-color: #d2e0f7; border: 2px solid #edf5ff; border-radius: 5px; box-shadow: rgb(255 255 255 / 40%) 0 5px 64px; font-size: 15px; margin: 60px auto 0 auto; max-width: 400px; padding: 5px 0 10px 0}\n";
$html_header .= "a.button {background-color: #627fca; border-radius: 5px; color: #e4e4e4; display: inline-block; margin-bottom: 5px; min-width: 120px; padding: 10px; width: 36%}\n";
$html_header .= "a.button:hover {background-color: #2f478a}\n";
$html_header .= "a.button.delete {background-color: #d8024b; color: #e4e4e4}\n";
$html_header .= "a.button.delete:hover {background-color: #ff1c69}\n";
$html_header .= ".footer {color: #5b75af; font-size: 12px; font-weight: bold; margin-top: 10px}\n";
$html_header .= ".footer > a {color: #2196f3}\n";
$html_header .= "p {font-weight: bold}\n";
$html_header .= "p.info {color: #37ab3c}\n";
$html_header .= "p.error {color: #e91e63}\n";
$html_header .= "p.button {margin: 10px 0}\n";
$html_header .= "@media only screen and (max-width: 600px) {.box {width: 100%}}\n";
$html_header .= "</style>\n";
$html_header .= "</head>\n";
$html_header .= "<body>\n";
$html_header .= "<div class=box>\n";
$html_header .= "<h1 class=title><a href=\"".$_SERVER['SCRIPT_NAME']."\">".$product['name']."</a></h1>\n";
$html_header .= "<h2 class=subtitle>".$product['info']."</h2>\n";
$html_header .= "<hr>\n";

// Display a divider line.
$html_footer = "<hr>\n";

// Display the footer bar - Begin.
$html_footer .= "<div class=footer>";
$html_footer .= "Version ".$product['version'];

// Check if the copyright should be displayed.
if ($display_copyright == 1) {

	// Display the copyright.
	$html_footer .= " | Copyright ".date('Y')." by <a href=\"".$company['link']."\" target=\"_blank\">".$company['name']."</a>\n";
}

// Display the footer bar - End.
$html_footer .= "</div>\n";

// Specify the HTML footer.
$html_footer .= "</div>\n";
$html_footer .= "</body>\n";
$html_footer .= "</html>\n";

// Sort the array of ZIP sources.
ksort($zip_source_urls);

// Check if no parameters were specified or if only the ZIP file should be deleted.
if (empty($_GET) || (isset($_GET['delzip']) && $_GET['delzip'] == 1)) {

	// Display the HTML header.
	echo $html_header;

	// Check if the WordPress ZIP file should be deleted.
	if (isset($_GET['delzip']) && $_GET['delzip'] == 1) {

		// Check if a WordPress ZIP file exist.
		if (file_exists($zip_local_path)) {

			// Delete the ZIP file.
			unlink($zip_local_path);

			// Check if the ZIP file could not be deleted.
			if (file_exists($zip_local_path)) {

				// Display an error message.
				echo "<p class=error>The ZIP file could not be deleted!</p>\n";

			// The ZIP file was deleted successfully.
			} else {

				// Display an information message.
				echo "<p class=info>The ZIP file was deleted successfully.</p>\n";
			}

		// No WordPress archive to delete.
		} else {

			// Display an information message.
			echo "<p class=error>No WordPress archive to delete.</p>\n";
		}

	// Check if a WordPress archive does exist.
	} elseif (file_exists($zip_local_path)) {

		// Display an information message.
		echo "<p class=info>Existing WordPress archive on host.</p>\n";

		// Display the links for the actual language.
		echo "<p><a class=\"button delete\" href=\"".$_SERVER['SCRIPT_NAME']."?delzip=1\">Delete Archive</a></p>\n";

	// No WordPress archive exists.
	} else {

		// Display an information message.
		echo "<p class=info>No existing WordPress archive on host.</p>\n";
	}

	// Display an information text.
	echo "<p>Please choose a Wordpress language:</p>\n";

	// Loop through all available languages.
	foreach($zip_source_urls as $key => $value){

		// Display the links for the actual language.
		echo "<p class=button><a class=button href=\"".$value."\">Download - ".strtoupper($key)."</a> <a class=button href=\"".$_SERVER['SCRIPT_NAME']."?lang=".$key."\">Installation - ".strtoupper($key)."</a></p>\n";
	}

	// Display the HTML footer.
	echo $html_footer;

// There are some parameters specified.
} else {

	// Check if the language parameter was specified.
	if (isset($_GET['lang']) && isset($zip_source_urls[strtolower($_GET['lang'])])) {

		// Get the language code.
		$language = strtolower($_GET['lang']);

	// The language parameter was not specified.
	} else {

		// User the default language code.
		$language = strtolower($default_language);
	}

	// Specify the ZIP source URL for the specified language.
	$zip_source_url = $zip_source_urls[$language];

	// Check if the delete parameter was specified.
	if (isset($_GET['del']) && $_GET['del'] == 1) {

		// Set the delete flag.
		$delete_zip = 1;

	// The delete parameter was not specified.
	} else {

		// Use the default delete value.
		$delete_zip = $default_delete_zip;
	}

	// Check if the overwrite parameter was specified.
	if (isset($_GET['over']) && $_GET['over'] == 1) {

		// Set the overwrite flag.
		$overwrite_wp = 1;

	// The overwrite parameter was not specified.
	} else {

		// Use the default overwrite value.
		$overwrite_wp = $default_overwrite_wp;
	}

	// Initialize the result.
	$curl_result = false;

	// Display an information message.
	$output = "<p class=info>Starting the Wordpress installation...</p>\n";

	// Check if there is already a WordPress installation in the actual directory.
	if (file_exists("wp-config.php") && $overwrite_wp == 0) {

		// Display an error message.
		$output .= "<p class=error>An active WordPress installation was found!</p>\n";

	// No active WordPress installation was found.
	} else {

		// Check if the ZIP file does not already exist.
		if (!file_exists($zip_local_path)) {

			// Display an information message.
			$output .= "<p class=info>Downloading the ZIP file...</p>\n";

			// Specify the ZIP resource to write the data to.
			$zip_resource = fopen($zip_local_path, "w");

			// Initialize the a cURL session.
			$curl = curl_init();

			// Specify the cURL parameters.
			curl_setopt($curl, CURLOPT_URL, $zip_source_url);
			curl_setopt($curl, CURLOPT_FAILONERROR, true);
			curl_setopt($curl, CURLOPT_HEADER, 0);
			curl_setopt($curl, CURLOPT_FOLLOWLOCATION, true);
			curl_setopt($curl, CURLOPT_AUTOREFERER, true);
			curl_setopt($curl, CURLOPT_BINARYTRANSFER,true);
			curl_setopt($curl, CURLOPT_TIMEOUT, 10);
			curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
			curl_setopt($curl, CURLOPT_FILE, $zip_resource);

			// Get The ZIP file from the destination URL.
			$curl_result = curl_exec($curl);

			// Close the cURL session
			curl_close($curl);

		// The ZIP file does exist.
		} else {

			// Display an information message.
			$output .= "<p class=info>The ZIP file already exists.</p>\n";

			// Set the result to true.
			$curl_result = TRUE;
		}

		// Check if the ZIP file could be downloaded.
		if ($curl_result) {

			// Check if the new ZIP file exist.
			if (file_exists($zip_local_path)) {

				// Display a success message.
				$output .= "<p class=info>Wordpress was downloaded successfully.</p>\n";

				// Check if the ZIP extension is enabled on the host.
				if (extension_loaded('zip')) {

					// Initialize a new ZIP object.
					$zip = new specialZipArchive;

					// Display an information message.
					$output .= "<p class=info>Opening the ZIP file...</p>\n";

					// Check if the ZIP file can be opened.
					if ($zip->open($zip_local_path) === TRUE){

						// Display an information message.
						$output .= "<p class=info>Decompressing the ZIP file...</p>\n";

						// Decompress the ZIP file.
						$errors = $zip->extractSubdirTo($zip_extract_dir, "wordpress/");

						// Close the ZIP object.
						$zip->close();

						// Check if there are no extracton errors.
						if (count($errors) == 0) {

							// Check if the ZIP file was successfully extracted.
							if (file_exists($zip_extract_dir.DIRECTORY_SEPARATOR."wp-settings.php")) {

								// Display a success message.
								$output .= "<p class=info>WordPress was extracted successfully.</p>\n";

								// Check if the ZIP file should be deleted.
								if ($delete_zip == 1) {

									// Delete the ZIP file.
									unlink($zip_local_path);

									// Check if the ZIP file could not be deleted.
									if (file_exists($zip_local_path)) {

										// Display an error message.
										$output .= "<p class=error>The ZIP file could not be deleted!</p>\n";

									// The ZIP file was deleted successfully.
									} else {

										// Display a success message.
										$output .= "<p class=info>The ZIP file was deleted successfully.</p>\n";
									}
								} // Check if the ZIP file should be deleted.

								// Redirect to the WordPress installation..
								header('Location: index.php');

								// Exit the script.
		     					exit();

							// Could not find the extrated files.
							} else {

								// Display an error message.
								$output .= "<p class=error>Could not find the extracted files!</p>\n";
							}

						// There were extraction errors.
						} else {

							// Display an error message.
							$output .= "<p class=error>There were extraction errors!</p>\n";
						}

					// The ZIP file could not be opened.
					} else {

						// Display an error message.
						$output .= "<p class=error>The ZIP file could not be opened!</p>\n";
					}

				// The ZIP extension is not enabled on the host.
				} else {

					// Display an error message.
					$output .= "<p class=error>The ZIP extension is not enabled!</p>\n";
				}

			// The new ZIP file can not be found.
			} else {

				// Display an error message.
				$output .= "<p class=error>The new ZIP file could not be found!</p>\n";
			}

		// There was an error downloading the file.
		} else {

			// Display an error message.
			$output .= "<p class=error>The ZIP file could not be downloaded!</p>\n";
		}
	} // No active WordPress installation was found.

	// Display the output.
	echo $html_header;
	echo $output;
	echo $html_footer;

} // There are some parameters specified.

// Class which extends the 'normal' class 'ZipArchive'.
// Code from: stanislav dot eckert at vizson dot de
// https://www.php.net/manual/en/ziparchive.extractto.php#116353
class specialZipArchive extends ZipArchive {

	// Function to extract files from a subfolder of the ZIP file.
	public function extractSubdirTo($destination, $subdir) {

		// Initialize the error array.
		$errors = array();

		// Prepare the path of the destination directory and set valid directory separators.
		$destination = str_replace(array("/", "\\"), DIRECTORY_SEPARATOR, $destination);

		// Prepare the path of the subdirectory and set valid directory separators.
		$subdir = str_replace(array("/", "\\"), "/", $subdir);

		// Check if the destination directory separator is not correct.
		if (substr($destination, mb_strlen(DIRECTORY_SEPARATOR, "UTF-8") * -1) != DIRECTORY_SEPARATOR) {

			// Set the correct directory separator.
			$destination .= DIRECTORY_SEPARATOR;
		}

		// Check if the subdirectory separator is not correct.
		if (substr($subdir, -1) != "/") {

			// Set the correct directory separator.
			$subdir .= "/";
		}

		// Loop through all the files.
		for ($i = 0; $i < $this->numFiles; $i++) {

			// Get the actual filename.
			$filename = $this->getNameIndex($i);

			// Check if the actual item is the specified subdirectory which has to be extracted.
			if (substr($filename, 0, mb_strlen($subdir, "UTF-8")) == $subdir) {

				// Get the realtive path of the item.
				$relativePath = substr($filename, mb_strlen($subdir, "UTF-8"));

				// Prepare the relative path of the item and set valid directory separators.
				$relativePath = str_replace(array("/", "\\"), DIRECTORY_SEPARATOR, $relativePath);

				// Check if the relative path is not empty.
				if (mb_strlen($relativePath, "UTF-8") > 0) {

					// Check if the item is a directory.
					if (substr($filename, -1) == "/") { // Directory

						// Check if the directory does not already exist in the destination.
						if (!is_dir($destination.$relativePath)) {

							// Check if a new directory can not be created.
							if (!@mkdir($destination.$relativePath, 0755, true)) {

								// Add the actual directory name to the error array.
								$errors[$i] = $filename;
							}
						} // Check if the directory does not exist in the destination.

					// The item is a file.
					} else {

						// Check if the file is not a 'dot-file'.
						if (dirname($relativePath) != ".") {

							// Check if the directory for the file does not already exist in the destination.
							if (!is_dir($destination.dirname($relativePath))) {

								// Create a new directory for the actual file.
								@mkdir($destination.dirname($relativePath), 0755, true);
							}
						}

						// Check if the new file could not be written to the destination directory.
						if (@file_put_contents($destination . $relativePath, $this->getFromIndex($i)) === false) {

							// Add the actual filename to the error array.
							$errors[$i] = $filename;
						}
					} // The item is a file.
				} // Check if the relative path is not empty.
			} // Check if the actual item is the specified subdirectory which has to be extracted.
		} // Loop through all the files.

		// Return the errors.
		return $errors;

	} // Function to extract files from a subfolder of the ZIP file.
} // Class which extends the 'normal' class 'ZipArchive'.

?>
