# WP-Start WordPress Start Script #
---

### Overview ###

**WP-Start** is a small script which can be uploaded to an empty directory on a webhost. Executing the script will download the newest version of Wordpress, decompress the ZIP file into the actual directory, delete the ZIP file and start the installation of WordPress. The script has a configuration section which is used to specify the WordPress default language and some other settings.

### Screenshots ###

![WP-Start - Menu](development/readme/wp-start_1.png "WP-Start - Menu")

### Setup ###

* Edit the configuration section of the file **wp-start.php**.
* Copy the file **wp-start.php** to your webhost (webdirectory).
* Be sure the file is in the directory where you want to install WordPress.
* Make sure that the installation directory is writable to the PHP/webserver process.
* Access the file **wp-start.php** from your webbrowser.
* Without any parameters the menu will be displayed.
* If you enter a parameter like '**auto**', the installation will begin without the menu.
* The automatic installation will use the default language specified in the configuration.
* As an example access the file like: '**wp-start.php?auto**' to use the automatic installation.

### Support ###

This is a free tool and support is not included and guaranteed. Nevertheless I will try to answer all your questions if possible. So write to my email address **biegel[at]gmx.ch** if you have a question :-)

### License ###

The **WP-Start** script is licensed under the [**MIT License (Expat)**](https://pb-soft.com/resources/mit_license/license.html) which is published on the official site of the [**Open Source Initiative**](https://opensource.org/licenses/MIT).
